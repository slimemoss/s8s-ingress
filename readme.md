# 概要
ロードバランサとかTLS終端とか

## 部品

### MetalLB
[MetalLB](https://metallb.universe.tf/)

#### Preparation
If you’re using kube-proxy in IPVS mode, since Kubernetes v1.14.2 you have to enable strict ARP mode.

Note, you don’t need this if you’re using kube-router as service-proxy because it is enabling strict arp by default.

```
kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl apply -f - -n kube-system
```

#### デプロイ
```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.6/manifests/namespace.yaml \
	-f https://raw.githubusercontent.com/metallb/metallb/v0.9.6/manifests/metallb.yaml \
	-f metallb/secret.yaml \
	-f metallb/configmap.yaml
```

### Nginx Ingress Controller
```
helmfile -f ingress-nginx/helmfile.yaml sync
```

### cert-manager
```
helmfile -f cert-manager/helmfile.yaml sync
sed -e "s/__API_TOKEN__/"(cat ./cert-manager/cloudflare_apitoken)"/g" ./cert-manager/cluster_issuer.yaml | kubectl apply -f -
```

#### Ingressサンプル
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: awesome-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-cloudflare"
spec:
  tls:
  - hosts:
    - awesome.slimemoss.com
    secretName: awesome-tls-secret
  rules:
  - host: awesome.slimemoss.com
    http:
      paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: awesome-service
              port:
                number: 80
```
